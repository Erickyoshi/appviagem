package br.edu.unisep.appTravel.controller.client;

import br.edu.unisep.appTravel.domain.dto.client.RegisterClientDto;
import br.edu.unisep.appTravel.domain.usecase.client.RegisterClientUseCase;
import br.edu.unisep.appTravel.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@AllArgsConstructor
@RestController
@RequestMapping("/client")
public class ClientController {

    private final RegisterClientUseCase registerClient;

    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>> save(@RequestBody RegisterClientDto client){
            registerClient.execute(client);
            return ResponseEntity.ok(DefaultResponse.of(true));
    }
}
