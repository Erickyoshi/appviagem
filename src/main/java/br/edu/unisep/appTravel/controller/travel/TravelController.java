package br.edu.unisep.appTravel.controller.travel;

import br.edu.unisep.appTravel.domain.dto.travel.RegisterTravelDto;
import br.edu.unisep.appTravel.domain.usecase.travel.RegisterTravelUseCase;
import br.edu.unisep.appTravel.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/travel")
public class TravelController {

    private final RegisterTravelUseCase registerTravel;

    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>> save(@RequestBody RegisterTravelDto travel){
        registerTravel.execute(travel);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }
}
