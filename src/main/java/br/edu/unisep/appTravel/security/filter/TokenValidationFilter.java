package br.edu.unisep.appTravel.security.filter;

import br.edu.unisep.appTravel.security.jwt.JwtProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TokenValidationFilter extends BasicAuthenticationFilter {

    private static final String HEADER_NAME = "Authorization";
    private static final String BEARER_PATTERN = "Bearer ";

    public TokenValidationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(HEADER_NAME);

        if (header != null && header.startsWith(BEARER_PATTERN)) {
            var authentication = authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken authenticate(HttpServletRequest request) {
        var token = request.getHeader(HEADER_NAME);

        if (token != null) {
            token = token.replace(BEARER_PATTERN, "");

            if (JwtProvider.isValid(token)) {
                var claims = JwtProvider.getClaims(token);
                var userData = (Map<String, Object>) claims.get("user");
                var roles = (List<String>) userData.get("roles");

                return new UsernamePasswordAuthenticationToken(claims.getSubject(), null,
                        roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
            }
        }

        return null;
    }
}
