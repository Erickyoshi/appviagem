package br.edu.unisep.appTravel.data.entity.travel;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "travel")
public class Travel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_travel")
    private Integer id;

    @Column(name = "country")
    private String country;

    @Column(name = "price")
    private Double price;

    @Column(name = "days")
    private Integer days;

    @Column(name = "name")
    private String name;

    @Column(name = "amount_people")
    private Integer amount_people;

    @Column(name = "description")
    private String description;

    @Column(name = "payment_method")
    private String payment_method;
}
