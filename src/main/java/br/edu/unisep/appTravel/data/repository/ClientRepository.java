package br.edu.unisep.appTravel.data.repository;


import br.edu.unisep.appTravel.data.entity.client.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
}
