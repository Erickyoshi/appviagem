package br.edu.unisep.appTravel.data.entity.role;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "roles")
public class Role {

    @Id
    @Column(name = "role_id")
    private String id;

    @Column(name = "role_name")
    private String name;

}
