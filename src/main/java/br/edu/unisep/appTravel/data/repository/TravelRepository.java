package br.edu.unisep.appTravel.data.repository;


import br.edu.unisep.appTravel.data.entity.travel.Travel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TravelRepository extends JpaRepository<Travel, Integer> {
}
