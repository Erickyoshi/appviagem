package br.edu.unisep.appTravel.data.entity.client;

import br.edu.unisep.appTravel.data.entity.travel.Travel;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_client")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private Integer phone;

    @Column(name = "address")
    private String address;

    @Column(name = "email")
    private String email;

    @OneToOne
    @JoinColumn(name = "id_travel", referencedColumnName = "id_travel")
    private Travel travel;

}
