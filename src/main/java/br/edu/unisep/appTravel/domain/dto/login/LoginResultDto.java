package br.edu.unisep.appTravel.domain.dto.login;

import br.edu.unisep.appTravel.domain.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LoginResultDto {

    private final UserDto userData;

    private final String token;

}
