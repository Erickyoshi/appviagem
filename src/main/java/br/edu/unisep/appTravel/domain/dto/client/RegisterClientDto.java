package br.edu.unisep.appTravel.domain.dto.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterClientDto {

    private String name;

    private Integer phone;

    private String address;

    private String email;

    private Integer travelId;
}
