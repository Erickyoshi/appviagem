package br.edu.unisep.appTravel.domain.dto.travel;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TravelDto {

    private Integer id;

    private String country;

    private Double price;

    private Integer days;

    private String name;

    private Integer amount_people;

    private String description;

    private String payment_method;

}
