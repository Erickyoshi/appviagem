package br.edu.unisep.appTravel.domain.dto.auth;

import lombok.Data;

@Data
public class AuthDto {

    private String login;

    private String password;

}
