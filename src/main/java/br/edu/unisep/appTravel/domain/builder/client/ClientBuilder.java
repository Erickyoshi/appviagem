package br.edu.unisep.appTravel.domain.builder.client;


import br.edu.unisep.appTravel.data.entity.client.Client;
import br.edu.unisep.appTravel.data.entity.travel.Travel;
import br.edu.unisep.appTravel.domain.dto.client.RegisterClientDto;
import org.springframework.stereotype.Component;

@Component
public class ClientBuilder {

    public Client from(RegisterClientDto registerClient){
        Client client = new Client();
        client.setName(registerClient.getName());
        client.setPhone(registerClient.getPhone());
        client.setAddress(registerClient.getAddress());
        client.setEmail(registerClient.getEmail());

        var travel = new Travel();
        travel.setId(registerClient.getTravelId());
        client.setTravel(travel);

        return client;
    }

}
