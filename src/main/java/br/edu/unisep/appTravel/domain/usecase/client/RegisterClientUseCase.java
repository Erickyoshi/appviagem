package br.edu.unisep.appTravel.domain.usecase.client;

import br.edu.unisep.appTravel.data.repository.ClientRepository;
import br.edu.unisep.appTravel.domain.builder.client.ClientBuilder;
import br.edu.unisep.appTravel.domain.dto.client.RegisterClientDto;
import br.edu.unisep.appTravel.domain.validator.client.RegisterClientValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterClientUseCase {

    private final RegisterClientValidator validator;
    private final ClientBuilder builder;
    private final ClientRepository repository;

    public void execute(RegisterClientDto registerClient){
        validator.validate(registerClient);

        var client = builder.from(registerClient);
        repository.save(client);
    }
}
