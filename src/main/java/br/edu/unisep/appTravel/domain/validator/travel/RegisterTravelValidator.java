package br.edu.unisep.appTravel.domain.validator.travel;

import br.edu.unisep.appTravel.domain.dto.travel.RegisterTravelDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;
import static br.edu.unisep.appTravel.domain.validator.ValidatorMessages.*;

@Component
public class RegisterTravelValidator {

    public void validate(RegisterTravelDto registerTravel){
        Validate.notBlank(registerTravel.getCountry(), MESSAGE_REQUIRED_TRAVEL_COUNTRY);
        Validate.notNull(registerTravel.getPrice(), MESSAGE_REQUIRED_TRAVEL_PRICE);
        Validate.notNull(registerTravel.getDays(), MESSAGE_REQUIRED_TRAVEL_DAYS);
        Validate.notBlank(registerTravel.getName(), MESSAGE_REQUIRED_TRAVEL_NAME);
        Validate.notBlank(registerTravel.getDescription(), MESSAGE_REQUIRED_TRAVEL_DESCRIPTION);
        Validate.notNull(registerTravel.getAmount_people(), MESSAGE_REQUIRED_TRAVEL_AMOUNT_PEOPLE);
        Validate.notBlank(registerTravel.getPayment_method(), MESSAGE_REQUIRED_TRAVEL_PAYMENT_METHOD);
    }
}
