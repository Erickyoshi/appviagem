package br.edu.unisep.appTravel.domain.builder.travel;

import br.edu.unisep.appTravel.data.entity.travel.Travel;
import br.edu.unisep.appTravel.domain.dto.travel.RegisterTravelDto;
import org.springframework.stereotype.Component;

@Component
public class TravelBuilder {

    public Travel from(RegisterTravelDto registerTravel){
        Travel travel = new Travel();
        travel.setCountry(registerTravel.getCountry());
        travel.setPrice(registerTravel.getPrice());
        travel.setDays(registerTravel.getDays());
        travel.setName(registerTravel.getName());
        travel.setAmount_people(registerTravel.getAmount_people());
        travel.setDescription(registerTravel.getDescription());
        travel.setPayment_method(registerTravel.getPayment_method());

        return travel;
    }
}
