package br.edu.unisep.appTravel.domain.validator;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidatorMessages {

    public static final String MESSAGE_REQUIRED_CLIENT_NAME = "Informe o nome do cliente";
    public static final String MESSAGE_REQUIRED_CLIENT_PHONE = "Informe o telefone para contato do cliente";
    public static final String MESSAGE_REQUIRED_CLIENT_ADDRESS = "Informe o endereço do cliente";
    public static final String MESSAGE_REQUIRED_CLIENT_EMAIL = "Informe o e-mail do cliente";
    public static final String MESSAGE_REQUIRED_CLIENT_TRAVEL = "Informe a viagem que será realizada";

    public static final String MESSAGE_REQUIRED_TRAVEL_COUNTRY = "Informe o país que será realizada a viagem";
    public static final String MESSAGE_REQUIRED_TRAVEL_PRICE = "Informe qual será o preço da viagem";
    public static final String MESSAGE_REQUIRED_TRAVEL_DAYS = "Informe quantos dias de duração terá a viagem";
    public static final String MESSAGE_REQUIRED_TRAVEL_NAME = "Informe o nome da cidade em que será a estadia da viagem";
    public static final String MESSAGE_REQUIRED_TRAVEL_AMOUNT_PEOPLE = "Informe quantas pessoas realizarão a viagem";
    public static final String MESSAGE_REQUIRED_TRAVEL_DESCRIPTION = "Informe a descrição do que será feito durante a viagem";
    public static final String MESSAGE_REQUIRED_TRAVEL_PAYMENT_METHOD = "Informe o método de pagamento da passagem";
}
