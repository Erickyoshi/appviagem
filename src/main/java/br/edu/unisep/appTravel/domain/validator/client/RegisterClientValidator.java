package br.edu.unisep.appTravel.domain.validator.client;

import br.edu.unisep.appTravel.domain.dto.client.RegisterClientDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.appTravel.domain.validator.ValidatorMessages.*;


@Component
public class RegisterClientValidator {

    public void validate(RegisterClientDto registerClient){
        Validate.notBlank(registerClient.getName(), MESSAGE_REQUIRED_CLIENT_NAME);
        Validate.notNull(registerClient.getPhone(), MESSAGE_REQUIRED_CLIENT_PHONE);
        Validate.notBlank(registerClient.getAddress(), MESSAGE_REQUIRED_CLIENT_ADDRESS);
        Validate.notBlank(registerClient.getEmail(), MESSAGE_REQUIRED_CLIENT_EMAIL);
        Validate.notNull(registerClient.getTravelId(), MESSAGE_REQUIRED_CLIENT_TRAVEL);
    }
}
