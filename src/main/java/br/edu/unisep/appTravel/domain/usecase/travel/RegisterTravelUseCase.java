package br.edu.unisep.appTravel.domain.usecase.travel;

import br.edu.unisep.appTravel.data.repository.TravelRepository;
import br.edu.unisep.appTravel.domain.builder.travel.TravelBuilder;
import br.edu.unisep.appTravel.domain.dto.travel.RegisterTravelDto;
import br.edu.unisep.appTravel.domain.validator.travel.RegisterTravelValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterTravelUseCase {

    private final RegisterTravelValidator validator;
    private final TravelBuilder builder;
    private final TravelRepository repository;

    public void execute(RegisterTravelDto registerTravel){
        validator.validate(registerTravel);

        var travel = builder.from(registerTravel);
        repository.save(travel);
    }

}
