package br.edu.unisep.appTravel.domain.dto.client;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientDto {

    private Integer id;

    private String name;

    private Integer phone;

    private String address;

    private String email;

    private String travel;
}
